import as.streams.data.Trader;
import as.streams.data.Transaction;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static java.util.Comparator.comparing;

public class StreamsTest {
    Trader mario = new Trader("Mario", "Milan");
    Trader alan = new Trader("Alan", "Cambridge");
    Trader brian = new Trader("Brian", "Cambridge");
    Trader raoul = new Trader("Raoul", "Cambridge");
    List<Transaction> transactions;

    @Before
    public void setup(){
        transactions = Arrays.asList(
                new Transaction(brian, 2011, 300),
                new Transaction(raoul, 2012, 1000),
                new Transaction(raoul, 2011, 400),
                new Transaction(mario, 2012, 710),
                new Transaction(mario, 2012, 700),
                new Transaction(alan, 2012, 950)
        );
    }

    @Test
    public void allTransactionsin2011SortByValue(){
        System.out.println("all transactions in 2011 sorted by value");
        transactions.stream().filter(transaction -> transaction.getYear()==2011)
                .sorted(comparing(Transaction::getValue))
                .forEach(System.out::println);
        System.out.println("===================");
    }

    @Test
    public void allTheUniqueCitiesTradersWorkIn(){
        System.out.println("allTheUniqueCitiesTradersWorkIn");

        transactions.stream().map(transaction -> transaction.getTrader().getCity())
                .distinct()
                .forEach(System.out::println);

        System.out.println("===================");
    }

    @Test
    public void allTradersFromCanbridgeAndSortByName(){
        System.out.println("allTradersFromCanbridgeAndSortByName");

        transactions.stream().filter(transaction ->                 "Cambridge".equals(transaction.getTrader().getCity()))
                .map(Transaction::getTrader)
                .distinct()
                .sorted(comparing(Trader::getName))
                .forEach(System.out::println);

        System.out.println("===================");

    }

    @Test
    public void allTradersNamesSortedAlphabetically(){
        System.out.println("allTradersNamesSortedAlphabetically");

        transactions.stream()
                .map(transaction -> transaction.getTrader().getName())
                .distinct()
                .sorted()
                .forEach(System.out::println);

        System.out.println("===================");

    }

    @Test
    public void anyTradersBasedInMilan(){
        System.out.println("anyTradersBasedInMilan");

        boolean found = transactions.stream()
                .anyMatch(transaction -> "Milan".equals(transaction.getTrader().getCity()));

        System.out.println("Found Trader from Milan");


        System.out.println("===================");

    }

    @Test
    public void allTransactionValuesFromTradersInCambridge(){
        System.out.println("allTransactionValuesFromTradersInCambridge");

        transactions.stream()
                .filter(transaction -> "Cambridge".equals(transaction.getTrader().getCity()))
                .map(Transaction::getValue)
                .forEach(System.out::println);

        System.out.println("===================");

    }

    @Test
    public void highestValueOfAllTransactions(){
        System.out.println("highestValueOfAllTransactions");

        transactions.stream()
                .map(Transaction::getValue)
                .reduce(Integer::max).ifPresent(System.out::println);


        System.out.println("===================");

    }

    @Test
    public void transactionWithSmallestValue(){
        System.out.println("transactionWithSmallestValue");

        transactions.stream()
                .reduce((t1, t2) -> t1.getValue() < t2.getValue() ? t1 : t2)
                .ifPresent(System.out::println);


        System.out.println("===================");

    }
}
